var tuxedo = angular.module('tuxedo', []);

tuxedo.controller('mainController', function($scope) {
    $scope.thisYear = (new Date()).getFullYear();
    $scope.toolbar = false;

    $scope.fbShare = function() {
        FB.ui({
            method: 'share',
            href: 'http://www.tuxedoinstitute.com',
        }, function(response) {});
    };

    $scope.twitterShare = function ( ) {
        var title = "Tuxedo Institute of the Philippines";
        var location = "http://www.tuxedoinstitute.com/";

        window.open('http://twitter.com/share?url=' + location + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    };

});

tuxedo.controller('formController', function($scope) {
    $scope.form = 1;
    $scope.isShown = false;
    $scope.selected = null;
    $scope.repeat = [];
    $scope.group = [];
    $scope.showRepeat = 0;
    $scope.validate = false;
    $scope.close = false;

    $scope.changeTab = function changeTab(tab) {
        $scope.form = tab;
    };
    $scope.addGroup = function addGroup(type) {
        $scope.repeat.push(type);
        $scope.validator();
    };
    $scope.selectedEmblem = function emblem(emblem) {
        $scope.selected = emblem;
        $scope.isShown = true;
        $scope.close = false;
    };
    $scope.closedReg = function closedReg(emblem) {
        $scope.selected = emblem;
        $scope.isShown = false;
        $scope.close = true;
    };
    $scope.finished = function finished(model, index, valid) {
        if (typeof model == "object") {
            if (Object.keys(model).length == 6 && checkEmpty(model)) {
                $scope.group[index] = model;
                $scope.showRepeat = -1;
            }
        }
        formValidate(valid, false);
        $scope.validator();

    };
    $scope.groupShow = function show(num, form) {
        $scope.showRepeat = num;
        formValidate(form, true);
    };
    $scope.capital = function capital(word) {
        return word.charAt(0).toUpperCase() + word.substr(1);
    };
    $scope.remove = function remove(index) {
        $scope.group.splice(index, 1);
        $scope.repeat.splice(index, 1);
        $scope.showRepeat = $scope.group.length;
        $scope.validator();
    };
    $scope.validator = function valid() {
        $scope.validate = ($scope.repeat.length > 0) && ($scope.repeat.length == $scope.group.length);
    };

    $scope.submit = function submit(event, valid) {
        if (!valid.$valid) {
            event.preventDefault();
            formValidate(valid, false);
        }
    };

    function formValidate(form, value) {
        angular.forEach(form, function(input) {
            input.$pristine = value;
        });
    }

    function checkEmpty(model) {
        for (var prop in model) {
            if (!model[prop]) return false;
        }
        return true;
    }
});