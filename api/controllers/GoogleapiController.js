/**
 * GoogleapiController
 *
 * @description :: Server-side logic for managing googleapis
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {	
	
	index: function ( req , res ) {
		res.view('home');
	},

	get: function ( req , res ) {
		var spreadsheet = Spreadsheet.getSheetOne;
		spreadsheet.receive(function ( err , rows , info ) {			
			var formated = Formater.formatExcel(rows);
			res.view('participants' , {rows: formated});
		});
	},

	add: function ( req , res ) {			
		var sheet = Spreadsheet.getSheet(req.body.emblem);
		var spreadsheet = Spreadsheet[sheet];
		Spreadsheet.getLastRows(spreadsheet , function ( rows ) {
			var student = Formater.toJsonData(req.body , rows[0] , 'student');
			var faculty = Formater.toJsonData(req.body , rows[1] , 'faculty');

			if (Object.keys(student).length) Send.spreadsheet(student , spreadsheet.student);
			if (Object.keys(faculty).length) Send.spreadsheet(faculty , spreadsheet.faculty);

			Send.email(req.body.email);
			res.redirect(res.locals.jlink('AppController' , 'process'));
		});
	},

	show: function ( req , res ) {
		res.view('add');
	}

};

