/**
 * AppController
 *
 * @description :: Server-side logic for managing apps
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
	index: function ( req , res ) {
		res.view('home' , {active: 'home'});
	},

	register: function ( req , res ) {
		res.view('register' , {active: 'register'});
	},

	contact: function ( req , res ) {
		res.view('contact-us' , {active: 'contact'})
	},

	process: function ( req , res ) {
		res.view('thank' , {active: 'home'});
	}

};

