/**
 * EllideratoController
 *
 * @description :: Server-side logic for managing ellideratoes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    index: function(req, res) {
        res.view('el-liderato/main', {
            active: 'main',
            title: 'El Liderato | Official Training Overview'
        });
    },

    challenges: function(req, res) {
        res.view('el-liderato/challenges', {
            active: 'challenges',
            title: 'El Liderato | National Leadership Challenges'
        });
    },

    discussions: function(req, res) {
        res.view('el-liderato/discussion', {
            active: 'discussions',
            title: 'El Liderato | National Leadership Discussions'
        });
    },

    organizers: function(req, res) {
        res.view('el-liderato/organizers', {
            active: 'organizers',
            title: 'El Liderato | National Organizers'
        });
    },

    venue: function(req, res) {
        res.view('el-liderato/venue', {
            active: 'venue',
            title: 'El Liderato | Official Venue'
        });
    },

    guidelines: function(req, res) {
        res.view('el-liderato/guidelines', {
            active: 'guidelines',
            title: 'El Liderato | National Guidelines'
        });
    },

    program: function(req, res) {
        res.view('el-liderato/program', {
            active: 'program',
            title: 'El Liderato | National Program'
        });
    },

    travelguide: function(req, res) {
        res.view('el-liderato/travelguide', {
            active: 'travelguide',
            title: 'El Liderato | Travel Guide'
        });
    }

};