/**
 * AdminController
 *
 * @description :: Server-side logic for managing admins
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	index: function ( req , res ) {
		res.redirect(res.locals.jlink('AdminController' , 'login'));
	},
	
	login: function ( req , res ) {
		var flash = req.flash('error');		
		var addClass = (flash.length) ? '' : 'hide';
		res.view('admin-page/admin-login' , {message: flash , alert: addClass});
	},

	dashboard: function ( req , res ) {
		if (req.method == "POST" && filter(req.body)) {
			res.view('admin-page/admin-dashboard');
		}else {
			var error = (req.method == "GET") ? 'Need Your Credentials' : 'Invalid Email or Password';
			req.flash('error' , error);
			res.redirect(res.locals.jlink('AdminController' , 'login'));
		}
	}	

};

function filter ( userCredentials ) {
	return (userCredentials.email == Credentials.email) && (userCredentials.password == Credentials.password);
}

