var time = require('time');

module.exports.stringify = function stringify ( json ) {
	return JSON.stringify(json , null , 4);
};

module.exports.toJsonData = function toJsonData ( json , currentRow , type ) {
	var obj = {};

	if (typeof json.type == "object") {		
		for (var index = 0 , len = json.type.length; index < len ; index ++){
			if (json.type[index] == type) {
				currentRow ++ ;
				obj[currentRow] = format({
					transId: json.transId,
					transAmount: json.transAmount,
					firstname: json.firstname[index],
					middlename: json.middlename[index],
					lastname: json.lastname[index],
					contact: json.contact[index],
					email: json.email[index],
					institution: json.institution[index],
					transBank: json.transBank				
				});		
			}	
		}
	}else if (json.type == type){
		currentRow ++ ;
		obj[currentRow] = format(json);
	}

	return obj;
};

module.exports.formatExcel = function formatExcel ( rows ) {
	var header = rows['1'];
	var transform = [];
	delete rows['1'];
	for (var property in rows) {
		var temp = {};
		for (var name in header) {
			temp[header[name]] = rows[property][name];
		}
		transform.push(temp);
	}
	return transform;
}

function format ( json ) {
	return {
		1: { val: json.transId },
		2: { val: json.transAmount },
		3: { val: json.firstname },
		4: { val: json.middlename },
		5: { val: json.lastname },
		6: { val: json.contact },
		7: { val: json.email },
		8: { val: json.institution },
		9: { val: dateAndTime() },
		10: { val: json.transBank }
	}
}

function dateAndTime () {
	var dateAndTime = new time.Date();
	dateAndTime.setTimezone('Asia/Manila');

	return dateAndTime.toLocaleDateString() + " " + dateAndTime.toLocaleTimeString();
}