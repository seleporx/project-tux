var https = require('https');
var postOptions = {
	host: 'mandrillapp.com',
	path: '/api/1.0/messages/send.json',
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	}
};

module.exports.email = function email( emails ) {
	var req = https.request(postOptions , function ( res ) {		
	});
	var write = JSON.stringify(emailData(emails));
	req.write(write);
	req.end();
};

module.exports.spreadsheet = function spreadsheet ( data , spreadsheet ) {
	spreadsheet.add(data);

	spreadsheet.send(function (err) {
		if (err) throw err;			
	});
};

function emailData ( email ) {

	return {
			key: 'xjEl0HBJZFdoaSg3scS1ew',
			message: {
				from_email: 'support@tuxedoinstitute.com',
				from_name: 'Tuxedo Institute of the Philippines',
				to: emailParser(email),
				auto_text: 'true',
				subject: 'To the Delegates',
				text: 'Your Registration Form has been Successfully Submitted .'
			}
	}
}

function emailParser ( email ) {
	var emailBlast = [];

	if (typeof email == "object") {
		email.forEach(function ( eachEmail ) {
			emailBlast.push({
				email: eachEmail,
				type: 'to'
			});
		});
	}else {
		emailBlast.push({
			email: email,
			type: 'to'
		});
	}

	return emailBlast;
}