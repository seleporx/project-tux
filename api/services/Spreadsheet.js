var Spreadsheet = require('edit-google-spreadsheet');
var credentials = require('./Credentials.js');
var async = require('async');

module.exports.getSheetOne = {};

Spreadsheet.load({
    debug: false,
    spreadsheetId: '1uc2jDrWz86yG4Dms_O0h_cvwsFo-alMF5atJBHWoqO4',
    worksheetName: 'Sheet1',
    username: credentials.email,
    password: credentials.password
}, function(err, spreadsheet) {

    module.exports.getSheetOne.student = spreadsheet;

});

Spreadsheet.load({
    debug: false,
    spreadsheetId: '1uc2jDrWz86yG4Dms_O0h_cvwsFo-alMF5atJBHWoqO4',
    worksheetName: 'Sheet2',
    username: credentials.email,
    password: credentials.password
}, function(err, spreadsheet) {

    module.exports.getSheetOne.faculty = spreadsheet;

});

module.exports.getSheet = function( sheet ) {
    switch ( sheet ) {
        case 'el-liderato':
            return 'getSheetOne';
        case 'le-maste':
            return 'getSheetTwo';
    }
};

module.exports.getLastRows = function( spreadsheet , callback ) {
    async.parallel([

        function( cb ) {
            spreadsheet.student.receive(function(err, rows, info) {                
                cb(null , info.lastRow);
            });
        },
        function( cb ) {
        	spreadsheet.faculty.receive(function(err, rows, info) {                
                cb(null , info.lastRow);
            });
        }

    ], function( err , results ) {
    	callback(results);
    });
};