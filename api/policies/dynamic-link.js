
module.exports = function ( req , res , next ) {
	res.locals.jlink = function ( controller , action ) {
		var routes = sails.config.routes;
		for (var url in routes) {
			if (routes[url].controller == controller && routes[url].action == action) {
				return url;
			}
		}
		return "/";
	};
	next();
}