/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `config/404.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

    /***************************************************************************
     *                                                                          *
     * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
     * etc. depending on your default view engine) your home page.              *
     *                                                                          *
     * (Alternatively, remove this and add an `index.html` file in your         *
     * `assets` directory)                                                      *
     *                                                                          *
     ***************************************************************************/

    '/': {
        controller: 'AppController'
    },

    '/get': {
        controller: 'GoogleapiController',
        action: 'get'
    },

    '/show': {
        controller: 'GoogleapiController',
        action: 'show'
    },

    '/registration/process': {
        controller: 'GoogleapiController',
        action: 'add'
    },

    '/register': {
        controller: 'AppController',
        action: 'register'
    },

    '/contact': {
        controller: 'AppController',
        action: 'contact'
    },
    '/process': {
        controller: 'AppController',
        action: 'process'
    },
    /*************El-Liderato****************/
    '/el-liderato': {
        controller: 'EllideratoController'
    },

    '/el-liderato/challenges': {
        controller: 'EllideratoController',
        action: 'challenges'
    },

    '/el-liderato/discussions': {
        controller: 'EllideratoController',
        action: 'discussions'
    },

    '/el-liderato/organizers': {
        controller: 'EllideratoController',
        action: 'organizers'
    },

    '/el-liderato/venue': {
        controller: 'EllideratoController',
        action: 'venue'
    },

    '/el-liderato/guidelines': {
        controller: 'EllideratoController',
        action: 'guidelines'
    },

    '/el-liderato/program': {
        controller: 'EllideratoController',
        action: 'program'
    },

    '/el-liderato/travelguide': {
        controller: 'EllideratoController',
        action: 'travelguide'
    },
    /*********** ADMIN ************/

    '/admin/login': {
        controller: 'AdminController',
        action: 'login'
    },

    '/admin/dashboard': {
        controller: 'AdminController',
        action: 'dashboard'
    }
    /*'/test': {
    controller: 'AppController',
    action: 'test'
  }*/

    /***************************************************************************
     *                                                                          *
     * Custom routes here...                                                    *
     *                                                                          *
     *  If a request to a URL doesn't match any of the custom routes above, it  *
     * is matched against Sails route blueprints. See `config/blueprints.js`    *
     * for configuration options and examples.                                  *
     *                                                                          *
     ***************************************************************************/

};